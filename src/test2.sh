#!/bin/bash
# Author: Chetan Vora chetan.vora@quintiles.com
# Usage: unzipJava parent-dir-for-zips output-dir password chunksize metadatadir
# unzipJava  . /u02/app/webadm/data/output/ "aPassword==" 50000 /u02/app/webadm/data/metadata
# echo "args are " $1 $2 $3 $4 $5
# set the path to the jarfile with CallableMain here
# this is branch, commit 2.39
# add comments
# test

export CLASSPATH=$CLASSPATH:/u02/app/GE/Data/bin/lib/*
echo "Start " $(date +"%Y-%m-%d %H:%M:%S,%3N")
logfilename=$(basename $2)
#this is branch, added commit 3.35
# added redirection to log file
java -cp $CLASSPATH com.q.winzipaes.main.CallableMain $1 $2 $3 $4 $5 > $logfilename 2>&1 &
echo "End " $(date +"%Y-%m-%d %H:%M:%S,%3N")
*****************************************************
# THIS BRANCH EDIT AFTER BRANCH2 IS CREATED 3:35 pM #
*****************************************************



